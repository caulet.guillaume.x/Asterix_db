use asterix_db;

-- 1.Liste des potions : Numéro, libellé, formule et constituant principal. (5 lignes)

    SELECT * FROM potion; 

-- 2.Liste des noms des trophées rapportant 3 points. (2 lignes)

    SELECT Nomcateg FROM categorie 
    WHERE NbPoints = 3;

-- 3.Liste des villages (noms) contenant plus de 35 huttes. (4 lignes)

    SELECT NomVillage FROM village 
    WHERE NbHuttes > 35;

-- 4.Liste des trophées (numéros) pris en mai / juin 52. (4 lignes)

   SELECT NumTrophee FROM trophee 
   WHERE DatePrise> CAST("2052-05-01 00:00:00" AS DATETIME) 
   AND datePrise< CAST("2052-06-30 00:00" AS DATETIME);

-- 5.Noms des habitants commençant par 'a' et contenant la lettre 'r'. (3 lignes)

    SELECT Nom FROM habitant 
    WHERE nom LIKE 'a%' 
    AND Nom LIKE '%r%';
    
-- 6.Numéros des habitants ayant bu les potions numéros 1, 3 ou 4. (8 lignes)

    SELECT DISTINCT NumHab FROM absorber 
    WHERE NumPotion= 1 OR NumPotion = 3 OR NumPotion = 4;

-- 7.Liste des trophées : numéro, date de prise, nom de la catégorie et nom du preneur. (10
-- lignes) 

    SELECT NumTrophee, DatePrise, NomCateg, Nom FROM categorie 
    INNER JOIN trophee ON trophee.codeCat = categorie.codeCat 
    INNER JOIN habitant ON trophee.NumPreneur = habitant.NumHab;
    
    
-- 8.Nom des habitants qui habitent à Aquilona. (7 lignes)

    SELECT Nom FROM habitant 
    INNER JOIN village ON habitant.NumVillage = village.NumVillage 
    WHERE village.NomVillage = 'Aquilona';

-- 9.Nom des habitants ayant pris des trophées de catégorie Bouclier de Légat. (2 lignes)

    SELECT Nom FROM habitant 
    INNER JOIN trophee ON habitant.NumHab = trophee.NumPreneur 
    INNER JOIN categorie ON trophee.CodeCat = categorie.CodeCat 
    WHERE categorie.NomCateg = 'Bouclier de Légat';

-- 10.Liste des potions (libellés) fabriquées par Panoramix : libellé, formule et constituant
-- principal. (3 lignes)

    SELECT LibPotion, Formule, ConstituantPrincipal FROM potion 
    INNER JOIN fabriquer ON fabriquer.NumPotion = potion.NumPotion   
    INNER JOIN habitant ON habitant.NumHab = fabriquer.NumHab 
    WHERE habitant.Nom = 'Panoramix';

-- 11.Liste des potions (libellés) absorbées  fabriquées par Homéopatix. (2 lignes)

    SELECT DISTINCT LibPotion FROM potion 
    INNER JOIN absorber ON potion.NumPotion = absorber.NumPotion 
    INNER JOIN  habitant ON absorber.NumHab = habitant.Numhab 
    WHERE habitant.Nom = 'Homéopatix';

-- 12.Liste des habitants (noms) ayant absorbé une potion fabriquée par l'habitant numéro
-- 3. (4 lignes)
    
    SELECT DISTINCT Nom FROM habitant 
    INNER JOIN absorber ON habitant.NumHab = absorber.NumHab 
    INNER JOIN fabriquer ON absorber.NumPotion = fabriquer.NumPotion 
    WHERE fabriquer.NumHab = 3;
   
-- 13.Liste des habitants (noms) ayant absorbé une potion fabriquée par Amnésix. (7 lignes)

    SELECT DISTINCT Nom FROM habitant 
    INNER JOIN absorber on habitant.NumHab = absorber.NumHab  
    INNER JOIN fabriquer on absorber.NumPotion = fabriquer.NumPotion 
    WHERE fabriquer.NumHab = (SELECT NumHab from habitant where Nom = "Amnésix");

-- 14.Nom des habitants dont la qualité n'est pas renseignée. (2 lignes)

    SELECT Nom FROM habitant 
    WHERE NumQualite is NULL;

-- 15.Nom des habitants ayant consommé la potion magique n°1 (c'est le libellé de la
-- potion) en février 52. (3 lignes)

    SELECT Nom FROM habitant 
    INNER JOIN absorber ON habitant.NumHab = absorber.NumHab 
    INNER JOIN potion ON absorber.NumPotion = potion.NumPotion 
    WHERE potion.LibPotion = 'potion magique n°1' 
    AND absorber.DateA > cast("2052-02-01 00:00:00" AS DATETIME) 
    AND absorber.DateA < cast("2052-02-29 00:00:00" AS DATETIME);

-- 16.Nom et âge des habitants par ordre alphabétique. (22 lignes)

    SELECT Nom, Age FROM habitant 
    ORDER BY nom;

-- 17.Liste des resserres classées de la plus grande à la plus petite : nom de resserre et nom
-- du village. (3 lignes)

    SELECT NomResserre, NomVillage FROM resserre 
    INNER JOIN village ON resserre.NumVillage = village.NumVillage 
    ORDER BY Superficie DESC;

-- ***
-- 18.Nombre d'habitants du village numéro 5. (4)

   SELECT Count(*) FROM habitant 
   WHERE NumVillage = 5;

-- 19.Nombre de points gagnés par Goudurix. (5)

    SELECT SUM(NbPoints) FROM categorie 
    INNER JOIN trophee ON categorie.CodeCat = trophee.CodeCat 
    INNER JOIN habitant ON trophee.NumPreneur = habitant.NumHab 
    WHERE habitant.Nom = "Goudurix";

-- 20.Date de première prise de trophée. (03/04/52)

    SELECT MIN(DatePrise) FROM trophee;

-- 21.Nombre de louches de potion magique n°2 (c'est le libellé de la potion) absorbées. (19)

    SELECT SUM(Quantite) FROM absorber 
    INNER JOIN potion ON absorber.NumPotion = potion.NumPotion 
    WHERE potion.LibPotion = "potion magique n°2";

-- 22.Superficie la plus grande. (895)

    SELECT MAX(Superficie) FROM resserre;

-- ***
-- 23.Nombre d'habitants par village (nom du village, nombre). (7 lignes)

    SELECT NomVillage, COUNT(*) FROM habitant 
    INNER JOIN village ON habitant.NumVillage = village.NumVillage 
    GROUP BY village.NomVillage;

-- 24.Nombre de trophées par habitant (6 lignes)

    SELECT Nom, COUNT(*) FROM trophee 
    INNER JOIN habitant ON trophee.NumPreneur = habitant.NumHab 
    GROUP BY habitant.Nom;

-- 25.Moyenne d'âge des habitants par province (nom de province, calcul). (3 lignes)

    SELECT NomProvince, AVG(Age) FROM province 
    INNER JOIN village ON province.NumProvince = village.NumProvince 
    INNER JOIN habitant ON village.NumVillage = habitant.NumVillage 
    GROUP BY NomProvince;

-- 26.Nombre de potions différentes absorbées par chaque habitant (nom et nombre). (9
-- lignes)

    SELECT nom, COUNT(DISTINCT NumPotion) FROM habitant 
    RIGHT JOIN absorber ON habitant.NumHab = absorber.NumHab GROUP BY habitant.nom;

-- 27.Nom des habitants ayant bu plus de 2 louches de potion zen. (1 ligne)

    SELECT nom FROM habitant 
    INNER JOIN absorber ON habitant.NumHab = absorber.NumHab 
    INNER JOIN potion ON absorber.NumPotion = potion.NumPotion 
    WHERE potion.LibPotion = 'Potion Zen' AND absorber.Quantite > 2;
-- ***
-- 28.Noms des villages dans lesquels on trouve une resserre (3 lignes)

    SELECT NomVillage FROM village 
    INNER JOIN resserre ON village.NumVillage = resserre.NumVillage;

-- 29.Nom du village contenant le plus grand nombre de huttes. (Gergovie)

    SELECT NomVillage FROM village 
    WHERE NbHuttes=(SELECT MAX(NbHuttes)FROM village);

-- 30.Noms des habitants ayant pris plus de trophées qu'Obélix (3 lignes).

    SELECT nom FROM (
    SELECT nom, COUNT(*) as nb FROM habitant 
    INNER JOIN trophee on trophee.numpreneur = habitant.numhab 
    GROUP BY trophee.numpreneur) as nom_nb 
    WHERE nb > (SELECT COUNT(*) as nb FROM habitant 
                INNER JOIN trophee on trophee.numpreneur = habitant.numhab
                WHERE habitant.nom LIKE 'Obélix'
                GROUP BY trophee.numpreneur);
